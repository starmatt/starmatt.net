export const transport = {
    host: process.env.MAIL_SERVER,
    port: process.env.MAIL_PORT || 587,
    secure: false, // upgrade later with STARTTLS
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_SECRET,
    },
};

export const to = process.env.MAIL_RECIPIENT;
