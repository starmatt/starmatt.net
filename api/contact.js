import express from 'express';
import nodemailer from 'nodemailer';
import isEmail from 'validator/lib/isEmail';
import { inHTMLData } from 'xss-filters';
import { stripIndents } from 'common-tags';

import { transport, to } from './mailer.config.js';

const app = express();
app.use(express.json());

app.get('/', (req, res) => res.status(405).send('Hic sunt dracones 🐉'));

const inputs = {
    email: (value) => !!value && isEmail(value),
    fullname: (value) => !!value && !!value.length,
    subject: (value) => !!value && !!value.length,
    message: (value) => !!value && !!value.length,
    phone: () => true,
    company: () => true,
};

const validate = (req, res, next) => {
    const isValid = Object.keys(inputs)
        .map((n) => inputs[n](req.body[n]))
        .every((n) => n);

    if (!isValid) {
        return res.status(422).json({ error: 'Request cannot be processed' });
    }

    return next();
};

const sendMail = (data) => {
    const now = new Date();
    const nm = nodemailer.createTransport(transport);

    return nm.sendMail({
        to,
        from: data.email,
        subject: `✨ New message: ${data.subject}`,
        text: stripIndents`
            📨 ${data.fullname} <${data.email}>
            📞 ${data.phone ? data.phone : '-'}
            🏢 ${data.company ? data.company : '-'}

            ~
            ${data.message}

            📅 ${now.toString()}
        `,
    });
};

app.post('/', validate, async (req, res) => {
    const sanitized = Object.keys(inputs).reduce((acc, n) => {
        acc[n] = inHTMLData(req.body[n]);
        return acc;
    }, {});

    try {
        await sendMail({ ...sanitized });
    } catch (e) {
        console.log(e);
        return res.status(500).json({ error: 'Internal server error' });
    }

    return res.json({ message: 'OK' });
});

export default {
    path: '/api/contact',
    handler: app,
};
