/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */

module.exports = {
    theme: {},
    variants: {
        borderColor: ['responsive', 'hover', 'focus', 'group-hover'],
        opacity: ['responsive', 'hover', 'focus', 'group-hover'],
    },
    corePlugins: {},
    plugins: [],
    purge: {
        // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
        enabled: process.env.NODE_ENV === 'production',
        content: [
            'components/**/*.vue',
            'layouts/**/*.vue',
            'pages/**/*.vue',
            'plugins/**/*.js',
            'nuxt.config.js',
        ],
        options: {
            whitelist: [
                'bg-center',
                'border-4',

                'border-gray-700',
                'border-green-500',
                'border-teal-500',
                'border-blue-500',
                'border-indigo-500',
                'border-purple-500',
                'border-pink-500',

                'text-green-300',
                'text-teal-300',
                'text-blue-300',
                'text-indigo-300',
                'text-purple-300',
                'text-pink-300',

                'text-yellow-900',
                'text-indigo-900',
                'text-blue-900',
                'text-gray-900',
                'text-green-900',

                'bg-yellow-300',
                'bg-indigo-300',
                'bg-blue-300',
                'bg-gray-300',
                'bg-green-300',
            ],
        },
    },
};
