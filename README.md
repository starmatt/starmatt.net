# starmatt.net

Personal website built with Vuejs and Nuxtjs. Visit at https://starmatt.net.

## Build setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start
