export const actions = {
    nuxtServerInit({ dispatch }, { $content }) {
        dispatch('content/init', $content);
    },
};
