export const state = () => ({
    projects: [],
    portfolio: [],
});

export const mutations = {
    SET_PROJECTS(state, { portfolio, projects }) {
        state.projects = projects;
        state.portfolio = portfolio;
    },
};

export const actions = {
    async init({ commit }, content) {
        const projects = await content('projects').sortBy('pos').fetch();
        const portfolio = await content('portfolio').sortBy('pos').fetch();
        commit('SET_PROJECTS', { portfolio, projects });
    },
};
