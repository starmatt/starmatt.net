import { en, fr } from './locales';

export default {
    /*
     ** Nuxt rendering mode
     ** See https://nuxtjs.org/api/configuration-mode
     */
    mode: 'universal',

    /*
     ** Nuxt target
     ** See https://nuxtjs.org/api/configuration-target
     */
    target: 'server',

    /*
     ** Headers of the page
     ** See https://nuxtjs.org/api/configuration-head
     */
    head: {
        htmlAttrs: {
            lang: 'en',
        },

        title: 'Fullstack web developer | Matthieu Borde | starmatt.net',

        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description,
            },
            {
                name: 'color-scheme',
                content: 'dark',
            },
        ],

        link: [
            {
                rel: 'apple-touch-icon',
                sizes: '180x180',
                href: '/favicon/apple-touch-icon.png',
            },
            {
                rel: 'icon',
                type: 'image/png',
                sizes: '32x32',
                href: '/favicon/favicon-32x32.png',
            },
            {
                rel: 'icon',
                type: 'image/png',
                sizes: '16x16',
                href: '/favicon/favicon-16x16.png',
            },
            { rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' },
            { rel: 'manifest', href: '/favicon/site.webmanifest' },
        ],
    },

    /*
     ** Global CSS
     */
    css: [],

    /*
     ** Plugins to load before mounting the App
     ** https://nuxtjs.org/guide/plugins
     */
    plugins: [
        /* { src: '~/plugins/i18n.js' } */
    ],

    /*
     ** Auto import components
     ** See https://nuxtjs.org/api/configuration-components
     */
    components: true,

    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/eslint-module',
        // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
        '@nuxtjs/tailwindcss',
    ],

    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://http.nuxtjs.org
        ['@nuxt/http', { https: process.env.HTTPS }],
        // Doc: https://i18n.nuxtjs.org
        [
            'nuxt-i18n',
            {
                seo: false,
                baseUrl: process.env.APP_URL || 'https://starmatt.net',
                locales: [
                    { code: 'en', iso: 'en-US' },
                    { code: 'fr', iso: 'fr-FR' },
                ],
                defaultLocale: 'fr',
                vueI18n: {
                    fallbackLocale: 'fr',
                    messages: { en, fr },
                },
            },
        ],
        // Doc: https://content.nuxtjs.org/
        '@nuxt/content',
        // Doc: https://github.com/nuxt-community/sitemap-module
        [
            '@nuxtjs/sitemap',
            {
                baseUrl: process.env.APP_URL || 'https://starmatt.net',
                i18n: 'fr',
                routes: [
                    {
                        url: '/',
                        links: ['en', 'fr'].map((lang) => ({
                            lang,
                            url: `${lang}/`,
                        })),
                    },
                ],
                exclude: ['/legal', '/sitemap'],
                defaults: {
                    changefreq: 'daily',
                    priority: 1,
                    lastmod: new Date(),
                },
            },
        ],
    ],

    /*
     ** Build configuration
     ** See https://nuxtjs.org/api/configuration-build
     */
    build: {},

    /*
     ** Router configuration
     ** See https://nuxtjs.org/api/configuration-router
     */
    router: {},

    /*
     ** Server Middleware
     ** See https://nuxtjs.org/api/configuration-servermiddleware
     */
    serverMiddleware: [
        // Contact form API
        '~/api/contact',
    ],
};
