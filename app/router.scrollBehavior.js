export default async (to) => {
    const findEl = (hash, x = 0) => {
        const promise = new Promise((resolve) => {
            if (x > 50) {
                return resolve(document.querySelector('#app'));
            }

            setTimeout(() => resolve(findEl(hash, ++x || 1)), 100);
        });

        return document.querySelector(hash) || promise;
    };

    if (to.hash) {
        const el = await findEl(to.hash);

        return window.scrollTo({ top: el.offsetTop, behavior: 'smooth' });
    }

    return { x: 0, y: 0 };
};
